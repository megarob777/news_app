//
//  News.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/26/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import UIKit
class Status: Decodable {
    let realmArticles: [RealmArticle]?
    
    enum CodingKeys: String, CodingKey {
        case articles
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        realmArticles = try? values.decode([RealmArticle].self, forKey: .articles)
    }
}
