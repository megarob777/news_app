//
//  RealmArticles.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/29/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class RealmArticle: Object, Decodable {
    @objc dynamic var author: String = ""
    @objc dynamic var descriptions: String = ""
    @objc dynamic var urlToImage: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var publishedAt: String = ""
    @objc dynamic var content: String = ""
    @objc dynamic var source: String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    enum CodingKeys: String, CodingKey {
        case author
        case description
        case urlToImage
        case publishedAt
        case content
        case url
        case title
        case name
        case id
        case source
    }
    
    convenience required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let author = (try? values.decode(String.self, forKey: .author)) ?? ""
        let descriptions = (try? values.decode(String.self, forKey: .description)) ?? ""
        let urlToImage = (try? values.decode(String.self, forKey: .urlToImage)) ?? ""
        let publishedAt = (try? values.decode(String.self, forKey: .publishedAt)) ?? ""
        let content = (try? values.decode(String.self, forKey: .content)) ?? ""
        let url = (try? values.decode(String.self, forKey: .url)) ?? ""
        let title = (try? values.decode(String.self, forKey: .title)) ?? ""
        let name = (try? values.decode(String.self, forKey: .name)) ?? ""
        let id = (try? values.decode(String.self, forKey: .id)) ?? ""
        let source = (try? values.decode(String.self, forKey: .source)) ?? ""
        

        self.init(author: author, description: descriptions, urlToImage: urlToImage, publishedAt: publishedAt, content: content, url: url, title: title, name: name, id: id, source: source)
    }
    
    convenience init(author: String, description: String, urlToImage: String, publishedAt: String, content: String, url: String, title: String, name: String, id: String, source: String) {
        self.init()
        self.author = author
        self.descriptions = description
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
        self.url = url
        self.title = title
        self.name = name
        self.id = id
        self.source = source
        
    } 
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
}
