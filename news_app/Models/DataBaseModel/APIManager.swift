//
//  APIManager.swift
//  news_app
//
//  Created by Роберт Кучинский on 9/6/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    static var instance = ApiManager()
    private enum Constants {
        static let baseURL = "https://newsapi.org"
    }
    
    private enum EndPoint {
        static let v2 = "/v2"
    }


    func getNews(parameters:[String: AnyHashable], onComplete: @escaping ([RealmArticle]?) -> Void) {
        let urlString = Constants.baseURL + EndPoint.v2
        URLSession.shared.dataTask(with: URL(string: "https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=8486f3d1895c47329df5b0b08704d989")!) { (data, urlResponse, error) in
            guard let data = data, error == nil, urlResponse != nil else {
                print("something wrong")
                onComplete(nil)
                return
            }
            print("downloaded")
            do {
                let decoder = JSONDecoder()
                let downloadedStatus = try decoder.decode(Status.self, from: data)
                if let downloadedArticles = downloadedStatus.realmArticles {
                    DispatchQueue.main.async {
                        onComplete(downloadedArticles)
                    }
                }
            } catch {
                print ("something wrong after downloading")
            }
            }.resume()


        AF.request(urlString, method: .get, parameters: parameters).responseJSON { (respons) in
            switch respons.result {
            case.success(let data):
                if let arrayV2 = data as? Array<Dictionary<String,Any>> {
                    var v2: [RealmArticle]
                    for v2Dict in arrayV2 {
                        let var2 = RealmArticle()
                        var2.author = v2Dict["author"] as? String ?? ""
                        var2.descriptions = v2Dict["descriptions"] as? String ?? ""
                        var2.title = v2Dict["title"] as? String ?? ""
                        var2.content = v2Dict["content"] as? String ?? ""
                        var2.name = v2Dict["name"] as? String ?? ""
                        var2.id = v2Dict["id"] as? String ?? ""
                        var2.publishedAt = v2Dict["publishedAt"] as? String ?? ""
                        var2.source = v2Dict["source"] as? String ?? ""
                    }
                }

            case .failure(_):
                print("nil")
            }
        }
    }
}

