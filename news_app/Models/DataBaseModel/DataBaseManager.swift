//
//  DataBaseManager.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/29/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseManager {
    static var instance = DataBaseManager()
    
    func saveToDataBase(_ objects: [RealmArticle]) {
        let realm = try! Realm()
        realm.beginWrite()
        realm.add(objects, update: .all)
        try? realm.commitWrite()
    }
    
    func getNewsFromDataBase() -> [RealmArticle] {
        
        let realm = try! Realm()
        let realmNewsResult = realm.objects(RealmArticle.self)
        return Array(realmNewsResult)
    }
    
    func getNewFromDataBase(withId id: String) -> RealmArticle? {
        let realm = try! Realm()
        let realmArticle = realm.object(ofType: RealmArticle.self, forPrimaryKey: id)
        return realmArticle
    }
}
