//
//  NewsListViewController.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/29/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import UIKit
import RealmSwift
import Kingfisher

class NewsListViewController: UIViewController {
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var openNewsLogo: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var newsNameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var publishedAtLabel: UILabel!
    @IBOutlet weak var scrollNewsText: UIScrollView!
    @IBOutlet weak var contentLabel: UILabel!
    
    var realmArticles = RealmArticle()
    var delegate: ViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        authorLabel.text = realmArticles.author
        descriptionLabel.text = realmArticles.descriptions
        newsNameLabel.text = realmArticles.name
        idLabel.text = realmArticles.id
        publishedAtLabel.text = realmArticles.publishedAt
        let url = URL(string: realmArticles.urlToImage)
        openNewsLogo.kf.setImage(with:url )
        contentLabel.text = realmArticles.content
    }
    
}
