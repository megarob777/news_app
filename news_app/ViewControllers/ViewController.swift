//
//  ViewController.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/26/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

protocol ViewControllerDelegate {
    func createNewsViewControllerDelegate (_ realmArticles: RealmArticle)
    
}

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBAction func showMoreClicked(_ sender: Any) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! NewsCell
//        tableView.estimatedRowHeight = 300
//        tableView.rowHeight = 300
        
//        let newsListViewController = UIViewController.getFromStoryboard(withId: "NewsListViewController")
//            as? NewsListViewController
//        newsListViewController?.delegate = self
//        navigationController?.pushViewController(newsListViewController!, animated: true)
    }
    
    
    //MARK: - Properties
    var searchNews = [String]()
    var realmArticles = [RealmArticle]()
    var delegate: ViewControllerDelegate?
    var selectedCell: Int = -1
    let limitOnPage: Int = 10
    
    let url = URL(string: "https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=8486f3d1895c47329df5b0b08704d989")
    
    //MARK: - Lifecicle
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadJson()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        tableView.refreshControl = refresher
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func downloadJson() {
        guard let downloadURL = url else { return }
        ApiManager.instance.getNews(parameters: ["pageSize": limitOnPage, "page": realmArticles.count/limitOnPage], onComplete: { [weak self] resultArray in
            guard let self = self else { return }
            if let resultArray = resultArray {
                self.realmArticles = resultArray
                self.tableView.reloadData()
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmArticles.count
    }
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as? NewsCell else {return UITableViewCell() }
        
        cell.newsName.text = realmArticles[indexPath.row].title
        
        cell.newsDescription.text = realmArticles[indexPath.row].descriptions
        
       
        if let imageURL = URL(string: realmArticles[indexPath.row].urlToImage ) {
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: imageURL)
                        if let data = data {
                            let image = UIImage(data: data)
                            DispatchQueue.main.async {
                                cell.newsLogo.image = image
                            }
                        }
                    }
                }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCell = indexPath.row
        tableView.reloadData()
        let newsVC = UIViewController.getFromStoryboard(withId: "NewsListViewController")
            as? NewsListViewController
        newsVC!.realmArticles = realmArticles[indexPath.row]
        navigationController?.pushViewController(newsVC!, animated: true)
    }
    
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        
        return refreshControl
    }()
    
    @objc
    func requestData() {
        print("requesting data")
        
        self.refresher.endRefreshing()
        
        tableView.reloadData()
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            realmArticles = DataBaseManager.instance.getNewsFromDataBase()
            tableView.reloadData()
            return
        }
        realmArticles = DataBaseManager.instance.getNewsFromDataBase().filter({ realmArticles -> Bool in
            return realmArticles.title.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height) + 1
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width) + 1
    }
}
