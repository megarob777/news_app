//
//  UIViewController+CoreKit.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/29/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import UIKit

extension UIViewController {
    static func getFromStoryboard(withId id: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: id)
        return controller
    }
}
