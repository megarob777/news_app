//
//  FirstViewController.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/27/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var startedImage: UIImageView!
    
    @IBAction func startedButtonClicked(_ sender: Any) {
//        let firstVC = UIViewController.getFromStoryboard(withId: "ViewController")
//            as? ViewController
//        navigationController?.pushViewController(firstVC!, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        startedImage.loadGif(name: "giphy")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
