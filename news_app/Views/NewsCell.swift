//
//  NewsCell.swift
//  news_app
//
//  Created by Роберт Кучинский on 8/26/19.
//  Copyright © 2019 Kuchynski. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    @IBOutlet weak var newsName: UILabel!
    @IBOutlet weak var newsLogo: UIImageView!
    @IBOutlet weak var newsDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
